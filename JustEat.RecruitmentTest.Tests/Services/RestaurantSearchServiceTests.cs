﻿using System.Linq;

namespace JustEat.RecruitmentTest.Tests.Services
{
    using System.Collections.Generic;

    using JustEat.RecruitmentTest.Exceptions;
    using JustEat.RecruitmentTest.Models;
    using JustEat.RecruitmentTest.Services;
    using JustEat.RecruitmentTest.Proxies;
    using JustEat.RecruitmentTest.Renderers;
    using JustEat.RecruitmentTest.Validators;

    using Moq;

    using NUnit.Framework;

    [TestFixture]
    public class RestaurantSearchServiceTests
    {
        private readonly Mock<IRestaurantServiceProxy> restaurantServiceMock;
        private readonly Mock<IValidator> validatorMock;
        private readonly Mock<IRenderer> rendererMock;

        private IRestaurantSearchService restaurantSearchService;

        public RestaurantSearchServiceTests()
        {
            this.restaurantServiceMock = new Mock<IRestaurantServiceProxy>(MockBehavior.Strict);
            this.validatorMock = new Mock<IValidator>(MockBehavior.Strict);
            this.rendererMock = new Mock<IRenderer>(MockBehavior.Strict);
        }

        [Test]
        public void Results_WhenInvalidPostcode_ThrowsInvalidPOstCodeException()
        {
            this.validatorMock.Setup(validator => validator.IsValid("x0x0x0x"))
                              .Returns(false);

            this.restaurantSearchService = new RestaurantSearchService(
                this.restaurantServiceMock.Object, this.validatorMock.Object, this.rendererMock.Object);

            try
            {
                var results = this.restaurantSearchService.Results("x0x0x0x").ToList();
                Assert.Fail("Operation returnerd results when it should throw an exception.");
            }
            catch (InvalidPostCodeException exception)
            {
                Assert.That(exception, Is.TypeOf<InvalidPostCodeException>());
                Assert.That(exception.Message, Is.EqualTo("Postcode is invalid or could not be found. Please rekey."));
            }

            this.validatorMock.VerifyAll();
        }

        [Test]
        public void Results_WhenValidPostcode_FindsAndRendersRestaurants()
        {
            var searchResult = new SearchApiResult
            {
                Restaurants = new List<SearchRestaurant>(1) {new SearchRestaurant()}
            };

            this.validatorMock.Setup(validator => validator.IsValid("SE19"))
                              .Returns(true);

            this.restaurantServiceMock.Setup(restaurantService => restaurantService.FindRestaurants("SE19"))
                                      .ReturnsAsync(searchResult);

            this.rendererMock.Setup(renderer => renderer.Render(searchResult.Restaurants))
                             .Returns(new List<string>() { "foo" });

            this.restaurantSearchService = new RestaurantSearchService(
                this.restaurantServiceMock.Object, this.validatorMock.Object, this.rendererMock.Object);

            var results = this.restaurantSearchService.Results("SE19").ToList();

            Assert.That(results, Is.Not.Null);

            this.validatorMock.VerifyAll();
            this.restaurantServiceMock.VerifyAll();
            this.rendererMock.VerifyAll();
        }
    }
}
