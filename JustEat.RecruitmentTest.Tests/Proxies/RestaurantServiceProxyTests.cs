﻿using System.Collections.Generic;
using JustEat.RecruitmentTest.Models;

namespace JustEat.RecruitmentTest.Tests.Proxies
{
    using System;
    using System.Net;
    using System.Threading;

    using JustEat.RecruitmentTest.Configuration;
    using JustEat.RecruitmentTest.Proxies;

    using Moq;

    using NUnit.Framework;

    using RestSharp;

    [TestFixture]
    public class RestaurantServiceProxyTests
    {
        private RestaurantServiceProxy restaurantService;
        private Mock<IApplicationConfiguration> applicationConfigurationMock;
        private Mock<IRestClient> clientMock;

        [SetUp]
        public void Setup()
        {
            this.applicationConfigurationMock = new Mock<IApplicationConfiguration>(MockBehavior.Strict);
            this.applicationConfigurationMock.SetupGet(applicationConfiguration => applicationConfiguration.RestaurantApiUrl)
                                             .Returns("http://localhost/api");

            this.applicationConfigurationMock.SetupGet(applicationConfiguration => applicationConfiguration.AcceptTenantHeader)
                                             .Returns("uk");

            this.applicationConfigurationMock.SetupGet(applicationConfiguration => applicationConfiguration.AcceptLanguageHeader)
                                             .Returns("en-GB");

            this.applicationConfigurationMock.SetupGet(applicationConfiguration => applicationConfiguration.AuthorizationHeader)
                                             .Returns("Basic 1234567");

            this.applicationConfigurationMock.SetupGet(applicationConfiguration => applicationConfiguration.HostHeader)
                                             .Returns("host");

            this.clientMock = new Mock<IRestClient>(MockBehavior.Strict);
            this.clientMock.SetupSet(client => client.BaseUrl = new Uri("http://localhost/api"));

            this.restaurantService = new RestaurantServiceProxy(this.applicationConfigurationMock.Object, this.clientMock.Object);
        }

        [Test]
        public void FindRestaurants_WhenSuccessfulResponse_ReturnsRestaurants()
        {
            this.clientMock.Setup(client => client.ExecuteTaskAsync<SearchApiResult>(It.IsAny<RestRequest>(), It.IsAny<CancellationToken>()))
                           .ReturnsAsync(new RestResponse<SearchApiResult>()
                           {
                               StatusCode = HttpStatusCode.OK,
                               Data = new SearchApiResult { Restaurants = new List<SearchRestaurant>(1) { new SearchRestaurant() } }
                           });

            Assert.That(this.restaurantService.FindRestaurants("SE19").Result.Restaurants.Count, Is.EqualTo(1));

            this.applicationConfigurationMock.VerifyAll();
            this.clientMock.VerifyAll();
        }

        [Test]
        public void FindRestaurants_WhenUnSuccessfulResponse_ReturnsNoRestaurants()
        {
            this.clientMock.Setup(client => client.ExecuteTaskAsync<SearchApiResult>(It.IsAny<RestRequest>(), It.IsAny<CancellationToken>()))
                           .ReturnsAsync(new RestResponse<SearchApiResult>() { StatusCode = HttpStatusCode.NotFound });

            Assert.That(this.restaurantService.FindRestaurants("SE19").Result.Restaurants.Count, Is.EqualTo(0));

            this.applicationConfigurationMock.VerifyAll();
            this.clientMock.VerifyAll();
        }
    }
}
