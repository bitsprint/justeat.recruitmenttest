﻿namespace JustEat.RecruitmentTest.Tests.Proxies
{
    using System;
    using System.Net;
    using System.Threading;

    using JustEat.RecruitmentTest.Configuration;
    using JustEat.RecruitmentTest.Proxies;

    using Moq;

    using NUnit.Framework;

    using RestSharp;

    [TestFixture]
    public class PostCodeServiceProxyTests
    {
        private PostCodeServiceProxy postCodeService;
        private Mock<IApplicationConfiguration> applicationConfigurationMock;
        private Mock<IRestClient> clientMock;

        [SetUp]
        public void Setup()
        {
            this.applicationConfigurationMock = new Mock<IApplicationConfiguration>(MockBehavior.Strict);
            this.applicationConfigurationMock.SetupGet(applicationConfiguration => applicationConfiguration.PostCodeApiUrl)
                                             .Returns("http://localhost/api");

            this.clientMock = new Mock<IRestClient>(MockBehavior.Strict);
            this.clientMock.SetupSet(client => client.BaseUrl = new Uri("http://localhost/api"));

            this.postCodeService = new PostCodeServiceProxy(this.applicationConfigurationMock.Object, this.clientMock.Object);
        }

        [Test]
        public void PostCodeExists_WhenSuccessfulResponse_ReturnsTrue()
        {
            this.clientMock.Setup(client => client.ExecuteTaskAsync(It.IsAny<RestRequest>(), It.IsAny<CancellationToken>()))
                           .ReturnsAsync(new RestResponse { StatusCode = HttpStatusCode.OK });

            Assert.That(this.postCodeService.PostCodeExists("SE19"), Is.True);

            this.applicationConfigurationMock.VerifyAll();
            this.clientMock.VerifyAll();
        }

        [Test]
        public void PostCodeExists_WhenUnSuccessfulResponse_ReturnsFalse()
        {
            this.clientMock.Setup(client => client.ExecuteTaskAsync(It.IsAny<RestRequest>(), It.IsAny<CancellationToken>()))
                           .ReturnsAsync(new RestResponse { StatusCode = HttpStatusCode.NotFound });

            Assert.That(this.postCodeService.PostCodeExists("X0X0"), Is.False);

            this.applicationConfigurationMock.VerifyAll();
            this.clientMock.VerifyAll();
        }
    }
}
