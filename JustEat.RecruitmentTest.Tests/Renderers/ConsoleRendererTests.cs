﻿namespace JustEat.RecruitmentTest.Tests.Renderers
{
    using System.Collections.Generic;
    using System.Linq;

    using JustEat.RecruitmentTest.Models;
    using JustEat.RecruitmentTest.Renderers;

    using NUnit.Framework;

    [TestFixture]
    public class ConsoleRendererTests
    {

        [Test]
        public void Render_WhenResults_RendersResults()
        {
            var results = new List<SearchRestaurant>
            {
                new SearchRestaurant
                {
                    Name = "Tony's Pizzeria",
                    CuisineTypes = new List<CuisineType>
                    {
                        new CuisineType
                        {
                            Name = "Italian"
                        },
                        new CuisineType
                        {
                            Name = "Pizza"
                        }
                    },
                    RatingStars = 5
                }
            };

            var rendered = new ConsoleRenderer().Render(results);

            Assert.That(rendered.First(), Is.EqualTo("Tony's Pizzeria".PadRight(55) + "Italian, Pizza".PadRight(40) + "5" + System.Environment.NewLine));
        }

        [Test]
        public void Render_WhenNoResults_RendersMessage()
        {
            var results = new List<SearchRestaurant>();

            var rendered = new ConsoleRenderer().Render(results);

            Assert.That(rendered.First(), Is.EqualTo("No restaurants were found in that area. Please try another postcode."));
        }
    }
}
