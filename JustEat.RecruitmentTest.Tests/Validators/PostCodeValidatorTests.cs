﻿namespace JustEat.RecruitmentTest.Tests.Validators
{
    using JustEat.RecruitmentTest.Proxies;
    using JustEat.RecruitmentTest.Validators;
    using Moq;
    using NUnit.Framework;

    [TestFixture]
    public class PostCodeValidatorTests
    {
        private readonly Mock<IPostCodeServiceProxy> postCodeServiceMock;
        private readonly PostCodeValidator postCodeValidator;

        public PostCodeValidatorTests()
        {
            this.postCodeServiceMock = new Mock<IPostCodeServiceProxy>(MockBehavior.Strict);
            this.postCodeValidator = new PostCodeValidator(postCodeServiceMock.Object);
        }

        public void IsValid_WhenServiceFindsPostcode_ReturnsTrue()
        {
            this.postCodeServiceMock.Setup(postCodeService => postCodeService.PostCodeExists(It.IsAny<string>()))
                                    .Returns(true);

            Assert.That(this.postCodeValidator.IsValid("foo"), Is.True);
        }

        public void IsValid_WhenServiceDoesNotFindPostcode_ReturnsFalse()
        {
            this.postCodeServiceMock.Setup(postCodeService => postCodeService.PostCodeExists(It.IsAny<string>()))
                                    .Returns(false);

            Assert.That(this.postCodeValidator.IsValid("bar"), Is.True);
        }
    }
}
