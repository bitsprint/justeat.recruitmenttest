﻿namespace JustEat.RecruitmentTest.AcceptanceTests
{
    using System.Collections.Generic;
    using System.Linq;
    using JustEat.RecruitmentTest.Dependencies;
    using JustEat.RecruitmentTest.Exceptions;
    using JustEat.RecruitmentTest.Services;
    using NUnit.Framework;
    using TinyIoC;

    using TechTalk.SpecFlow;

    [Binding]
    public sealed class CodingTestSteps
    {
        private IServiceLocator<TinyIoCContainer> ServiceLocator => ScenarioContext.Current["ServiceLocator"] as IServiceLocator<TinyIoCContainer>;

        [Given(@"I have entered the outcode (.*)")]
        public void GivenIHaveEnteredTheOutcode(string outcode)
        {
            var serviceLocator = new TinyIocServiceLocator();

            Bootstrap.RegisterDependencies(serviceLocator);

            ScenarioContext.Current["ServiceLocator"] = serviceLocator;
            ScenarioContext.Current["Search"] = outcode;
        }

        [When(@"I search for results")]
        public void WhenISearchForResults()
        {
            var restaurantService = ServiceLocator.Container.Resolve<IRestaurantSearchService>();
            try
            {
                ScenarioContext.Current["Results"] = restaurantService.Results(ScenarioContext.Current["Search"].ToString()).ToList();
            }
            catch (InvalidPostCodeException exception)
            {
                ScenarioContext.Current["Results"] = exception;
            }
        }

        [Then(@"a no results message is displayed")]
        public void ThenANoResultsMessageIsDisplayed()
        {
            var exception = ScenarioContext.Current["Results"] as InvalidPostCodeException;
            Assert.That(exception, Is.Not.Null);
            Assert.That(exception.Message, Is.EqualTo("Postcode is invalid or could not be found. Please rekey.")); 
        }

        [Then(@"the Name, Cuisine Types and Rating of the restaurant are displayed")]
        public void ThenTheNameCuisineTypesAndRatingOfTheRestaurantAreDisplayed()
        {
            var results = ScenarioContext.Current["Results"] as IEnumerable<string>;
            var result = results.First();
            Assert.That(results.Count, Is.AtLeast(1));
            Assert.That(result.Substring(0, 55), Is.Not.Empty);
            Assert.That(result.Substring(56, 40), Is.Not.Empty);
            Assert.That(result.Substring(97, 3), Is.Not.Empty);
        }
    }
}
