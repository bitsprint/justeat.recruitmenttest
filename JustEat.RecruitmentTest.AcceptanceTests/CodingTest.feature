﻿Feature: CodingTest
	As a user running the application
	I can view a list of restaurants in a user submitted outcode (ex. SE19)
	So that I know which restaurants are currently available
	
Scenario: Search for restaurants valid postcode
	Given I have entered the outcode se19
	When I search for results
	Then the Name, Cuisine Types and Rating of the restaurant are displayed

Scenario: Search for restaurants invalid postcode
	Given I have entered the outcode x0x0x0
	When I search for results
	Then a no results message is displayed