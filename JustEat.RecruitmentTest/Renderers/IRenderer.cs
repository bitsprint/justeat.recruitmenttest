﻿using System.Collections.Generic;
using JustEat.RecruitmentTest.Models;

namespace JustEat.RecruitmentTest.Renderers
{
    public interface IRenderer
    {
        IEnumerable<string> Render(IEnumerable<SearchRestaurant> searchResults);
    }
}