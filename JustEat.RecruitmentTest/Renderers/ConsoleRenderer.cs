﻿namespace JustEat.RecruitmentTest.Renderers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using JustEat.RecruitmentTest.Models;

    public class ConsoleRenderer : IRenderer
    {
        public IEnumerable<string> Render(IEnumerable<SearchRestaurant> searchResults)
        {
            if (!searchResults.Any())
            {
                yield return "No restaurants were found in that area. Please try another postcode.";
            }

            foreach (var searchRestaurant in searchResults)
            {
                yield return $"{searchRestaurant.Name.PadRight(55)}{string.Join(", ", searchRestaurant.CuisineTypes.Select(ct => ct.Name)).PadRight(40)}{searchRestaurant.RatingStars}{Environment.NewLine}";
            }
        }
    }
}
