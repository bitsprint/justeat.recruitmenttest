﻿namespace JustEat.RecruitmentTest.Dependencies
{
    public interface IServiceLocator<out T>
    {
        T Container { get; }
    }
}