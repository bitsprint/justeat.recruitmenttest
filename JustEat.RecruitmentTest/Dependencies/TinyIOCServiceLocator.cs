﻿namespace JustEat.RecruitmentTest.Dependencies
{
    using System;

    using TinyIoC;

    public class TinyIocServiceLocator : IServiceLocator<TinyIoCContainer>
    {
        internal static Func<TinyIoCContainer> container = () => TinyIoCContainer.Current;

        public TinyIoCContainer Container => container();
    }
}
