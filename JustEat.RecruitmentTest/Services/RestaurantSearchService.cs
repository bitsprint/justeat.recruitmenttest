﻿namespace JustEat.RecruitmentTest.Services
{
    using System.Collections.Generic;

    using JustEat.RecruitmentTest.Exceptions;
    using JustEat.RecruitmentTest.Proxies;
    using JustEat.RecruitmentTest.Renderers;
    using JustEat.RecruitmentTest.Validators;

    public class RestaurantSearchService : IRestaurantSearchService
    {
        private readonly IRestaurantServiceProxy restaurantService;
        private readonly IValidator validator;
        private readonly IRenderer renderer;

        public RestaurantSearchService(
            IRestaurantServiceProxy restaurantService, 
            IValidator validator,
            IRenderer renderer)
        {
            this.restaurantService = restaurantService;
            this.validator = validator;
            this.renderer = renderer;
        }

        public IEnumerable<string> Results(string searchParameters)
        {
            if (this.validator.IsValid(searchParameters) == true)
            {
                var searchResult = this.restaurantService.FindRestaurants(searchParameters).Result;

                foreach (var line in renderer.Render(searchResult.Restaurants))
                {
                    yield return line;
                }
            }
            else
            {
                throw new InvalidPostCodeException();
            }
        }
    }
}
