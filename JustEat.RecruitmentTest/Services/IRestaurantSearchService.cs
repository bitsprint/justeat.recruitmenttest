﻿namespace JustEat.RecruitmentTest.Services
{
    using System.Collections.Generic;

    public interface IRestaurantSearchService
    {
        IEnumerable<string> Results(string searchParameters);
    }
}