﻿namespace JustEat.RecruitmentTest.Exceptions
{
    using System;

    public class InvalidPostCodeException : Exception
    {
        public InvalidPostCodeException() : base("Postcode is invalid or could not be found. Please rekey.")
        {
        }
    }
}
