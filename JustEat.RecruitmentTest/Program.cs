﻿namespace JustEat.RecruitmentTest
{
    using System;

    using JustEat.RecruitmentTest.Dependencies;
    using JustEat.RecruitmentTest.Exceptions;
    using JustEat.RecruitmentTest.Services;

    using TinyIoC;

    public class Program
    {
        internal static Func<IServiceLocator<TinyIoCContainer>> ServiceLocatorFactory = () => new TinyIocServiceLocator();

        private static readonly IServiceLocator<TinyIoCContainer> ServiceLocator = ServiceLocatorFactory();

        public static void Main(string[] args)
        {
            try
            {
                Bootstrap.RegisterDependencies(ServiceLocator);

                var restaurantSearchService = ServiceLocator.Container.Resolve<IRestaurantSearchService>();

                var searchParameters = SearchPrompt();

                while (!string.IsNullOrWhiteSpace(searchParameters))
                {
                    try
                    {
                        foreach (var result in restaurantSearchService.Results(searchParameters))
                        {
                            Console.WriteLine(result);
                        }
                    }
                    catch (InvalidPostCodeException exception)
                    {
                        Console.WriteLine(exception.Message);
                    }

                    searchParameters = SearchPrompt();
                }
            }
            catch (Exception exception)
            {
                // Log the exception, e.g.
                // Logger.Error(exception)

                Console.WriteLine("An error occurred and the administrator has been notified. Please press any key to exit.");
                Console.ReadLine();
            }
        }

        private static string SearchPrompt()
        {
            Console.WriteLine("Please enter a valid outcode (e.g. SE19):");
            return Console.ReadLine();
        }
    }
}
