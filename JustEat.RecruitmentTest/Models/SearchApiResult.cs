﻿namespace JustEat.RecruitmentTest.Models
{
    using System.Collections.Generic;

    public class SearchApiResult
    {
        public SearchApiResult()
        {
            this.Restaurants = new List<SearchRestaurant>();    
        }

        public List<SearchRestaurant> Restaurants { get; set; }
    }
}
