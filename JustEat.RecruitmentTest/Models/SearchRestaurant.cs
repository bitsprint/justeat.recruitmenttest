﻿namespace JustEat.RecruitmentTest.Models
{
    using System.Collections.Generic;

    public class SearchRestaurant
    {
        public string Name { get; set; }

        public double RatingStars { get; set; }

        public List<CuisineType> CuisineTypes { get; set; }
    }
}
