﻿using System;
using JustEat.RecruitmentTest.Validators;

namespace JustEat.RecruitmentTest.Models
{
    public class InputModel
    {
        private bool isValid;

        public InputModel(string outcode)
        {
            this.Outcode = outcode;
            this.isValid = false;
        }

        public string Outcode { get; private set; }

        internal void Validate(IValidator validator)
        {
            validator.Validate(this.Outcode);
            this.isValid = true;
        }

        public bool IsValid()
        {
            return isValid;
        }
    }
}
