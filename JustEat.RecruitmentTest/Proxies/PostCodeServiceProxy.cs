﻿namespace JustEat.RecruitmentTest.Proxies
{
    using System;
    using System.Net;
    using System.Threading;
    using System.Threading.Tasks;
    using JustEat.RecruitmentTest.Configuration;
    using RestSharp;

    public class PostCodeServiceProxy : IPostCodeServiceProxy
    {
        private readonly IApplicationConfiguration applicationConfiguration;
        private readonly IRestClient client;

        public PostCodeServiceProxy(IApplicationConfiguration applicationConfiguration, IRestClient client)
        {
            this.applicationConfiguration = applicationConfiguration;
            this.client = client;

            client.BaseUrl = new Uri(this.applicationConfiguration.PostCodeApiUrl);
        }

        public bool PostCodeExists(string postcode)
        {
            var cancellationTokenSource = new CancellationTokenSource();

            var request = new RestRequest("outcodes/{postcode}");

            request.AddParameter("postcode", postcode, ParameterType.UrlSegment);

            var response = client.ExecuteTaskAsync(request, cancellationTokenSource.Token);

            return response.Result.StatusCode == HttpStatusCode.OK;
        }
    }
}
