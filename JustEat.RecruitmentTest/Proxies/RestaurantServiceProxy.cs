﻿using System.Net;

namespace JustEat.RecruitmentTest.Proxies
{
    using System;
    using System.Threading;
    using System.Threading.Tasks;

    using JustEat.RecruitmentTest.Configuration;
    using JustEat.RecruitmentTest.Models;

    using RestSharp;

    public class RestaurantServiceProxy : IRestaurantServiceProxy
    {
        private readonly IApplicationConfiguration applicationConfiguration;
        private readonly IRestClient client;

        public RestaurantServiceProxy(IApplicationConfiguration applicationConfiguration, IRestClient client)
        {
            this.client = client;
            this.applicationConfiguration = applicationConfiguration;
            client.BaseUrl = new Uri(applicationConfiguration.RestaurantApiUrl);
        }

        public async Task<SearchApiResult> FindRestaurants(string query)
        {
            var cancellationTokenSource = new CancellationTokenSource();

            var request = new RestRequest("restaurants?q={query}");

            request.AddParameter("query", query, ParameterType.UrlSegment);

            request.AddHeader("Accept-Tenant", applicationConfiguration.AcceptTenantHeader);
            request.AddHeader("Accept-Language", applicationConfiguration.AcceptLanguageHeader);
            request.AddHeader("Authorization", applicationConfiguration.AuthorizationHeader);
            request.AddHeader("Host", applicationConfiguration.HostHeader);

            var response = await client.ExecuteTaskAsync<SearchApiResult>(request, cancellationTokenSource.Token);

            return response != null && response.StatusCode == HttpStatusCode.OK
                ? response.Data
                : new SearchApiResult();
        }
    }
}
