﻿using System.Threading.Tasks;

namespace JustEat.RecruitmentTest.Proxies
{
    public interface IPostCodeServiceProxy
    {
        bool PostCodeExists(string postcode);
    }
}