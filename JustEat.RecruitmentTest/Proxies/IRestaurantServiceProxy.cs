﻿namespace JustEat.RecruitmentTest.Proxies
{
    using System.Threading.Tasks;

    using JustEat.RecruitmentTest.Models;

    public interface IRestaurantServiceProxy
    {
        Task<SearchApiResult> FindRestaurants(string query);
    }
}