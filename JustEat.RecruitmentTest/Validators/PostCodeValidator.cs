﻿namespace JustEat.RecruitmentTest.Validators
{
    using JustEat.RecruitmentTest.Proxies;

    public class PostCodeValidator : IValidator
    {
        private readonly IPostCodeServiceProxy postCodeService;

        public PostCodeValidator(IPostCodeServiceProxy postCodeService)
        {
            this.postCodeService = postCodeService;
        }

        public bool IsValid(string postcode)
        {
            var result = this.postCodeService.PostCodeExists(postcode);
            return result;
        }
    }
}
