namespace JustEat.RecruitmentTest.Validators
{
    public interface IValidator
    {
        bool IsValid(string stringToValidate);
    }
}