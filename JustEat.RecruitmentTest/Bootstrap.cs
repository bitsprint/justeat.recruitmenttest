﻿namespace JustEat.RecruitmentTest
{
    using System.Configuration;

    using Castle.Components.DictionaryAdapter;

    using JustEat.RecruitmentTest.Configuration;
    using JustEat.RecruitmentTest.Dependencies;

    using RestSharp;

    using TinyIoC;

    public static class Bootstrap
    {
        public static void RegisterDependencies(IServiceLocator<TinyIoCContainer> serviceLocator)
        {
            serviceLocator.Container.AutoRegister();
            serviceLocator.Container.Register<IRestClient>((c,p) => new RestClient());
            serviceLocator.Container.Register(new DictionaryAdapterFactory().GetAdapter<IApplicationConfiguration>(ConfigurationManager.AppSettings));
        }
    }
}
