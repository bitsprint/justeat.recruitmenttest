﻿namespace JustEat.RecruitmentTest.Configuration
{
    public interface IApplicationConfiguration
    {
        string RestaurantApiUrl { get; set; }

        string PostCodeApiUrl { get; set; }

        string AcceptTenantHeader { get; set; }

        string AcceptLanguageHeader { get; set; }

        string AuthorizationHeader { get; set; }

        string HostHeader { get; set; }
    }
}
